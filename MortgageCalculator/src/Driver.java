/*  
 *  Name: Apal Bansal
 *  Date: 3/6/13
 *  Purpose: To demonstrate "good java code" practice while implementing a basic payment and interest calculator.  
 */

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Scanner;

/*
 * Class with main method that drives user input; this is where console input or GUI input code would be written.
 * Since all Mortgage calculation occurs within Mortgage object, implementing console, GUI, or even applet / servlet
 * variances is trivial; simply make the appropriate medium and create a Mortgage object to manipulate.
 */
public class Driver {

	public static void main(String[] args) throws CloneNotSupportedException {
		
			//control and input variables
			boolean control = true;
			String confirm = "";
			Scanner input = new Scanner(System.in);
			
			//while loop that continually prompts for input as long as the user doesn't enter yes
			while(control){	
				
				/* 
				 * inputs, stored as Doubles, surrounded by a basic try catch to catch any non number input
				 * if a GUI is preferred, this try catch must be transferred to encapsulate 
				 * the input relevant GUI elements
				 */
			 	try {
					System.out.println("Please enter the price (in dollars, without commas and without a $ sign) " +
										"\n(If you enter 0 for the price or term length values, " +
										"the program will default those values to 1) : ");
					Double price = input.nextDouble();
					System.out.println("Please enter the percent down payment " +
							"(If it is 5.5%, please enter 5.5, not .055): ");
					Double downPayment = input.nextDouble();
					System.out.println("Please enter the annual interest rate " +
							"(If it is 5.5%, please enter 5.5, not .055): ");
					Double rate = input.nextDouble();
					System.out.println("Please enter the length of the term (in years): ");
					Double term = input.nextDouble();
					
					//create a mortgage object and print relevant information based on parameters
					Mortgage m = new Mortgage(price, downPayment, rate, term);
					System.out.println(m.toString());
				} 
			 	catch (Exception e) {
					System.out.println("\nYou must input number values without letters or symbols like commas or $");
					confirm = input.next();
				}
			    
			    System.out.println("\nWould you like to enter again? Please type 'yes' to continue entering input.");
			    confirm = input.next();
			    
			    if(!confirm.toUpperCase().equals("YES")) control = false;
			}
		    
		    input.close();
	}
	
	
	/**************************************************************/
	
	
	/*
	 * Mortgage object class, responsible for calculating appropriate values 
	 * and implementing ability to print information, clone, and compare other Mortgage objects
	 */
	static class Mortgage implements Comparable<Object>, Cloneable, Serializable {
		
		//CLASS VARIABLES
		
		private static final long serialVersionUID = 1L;
		Double price, downPayment, rate, term;
		
		/**************************************************************/
		
		//CONSTRUCTORS
		
		//no argument constructor sets arbitrary default values
		public Mortgage() {
			super();
			this.price = 1.0;
			this.downPayment = 1.0;
			this.rate = 1.0;
			this.term = 1.0;
		}
		
		/*
		 * 4 argument constructor uses passed initial price, down payment percent, 
		 * initial rate percent, and term of loan as parameters (adds 1 to price and term values if they are 0)
		 */
		public Mortgage(Double price, Double downPayment, Double rate,
				Double term) {
			this.price = price;
			if(this.price == 0) this.price = 1.0;
			this.downPayment = downPayment;
			this.rate = rate;
			//since an annual rate of 0% is feasible but the formula errors if it is 0%, just make rate miniscule
			if(this.rate == 0) this.rate = 0.000001; 
			this.term = term;
			if(this.term == 0) this.term = 1.0;
		}
		
		/**************************************************************/
		
		//MUTATOR (get/set) METHODS
		
		//get the price
		public Double getPrice() {
			return price;
		}

		//set the price
		public void setPrice(Double price) {
			this.price = price;
		}

		//get the down payment percentage
		public Double getDownPayment() {
			return downPayment;
		}

		//set the down payment percentage
		public void setDownPayment(Double downPayment) {
			this.downPayment = downPayment;
		}

		//get the annual rate percentage
		public Double getRate() {
			return rate;
		}

		//set the annual rate percentage
		public void setRate(Double rate) {
			this.rate = rate;
		}

		//get the duration of the loan
		public Double getTerm() {
			return term;
		}

		//set the duration of the loan (in years)
		public void setTerm(Double term) {
			this.term = term;
		}
		
		/**************************************************************/
		
		//USER-DEFINED METHODS
		
		//gets the down payment amount based on initial price and down payment percentage
		public Double calculateDownPayment(){
			return getPrice() * getDownPayment()/100;
		}
		
		//gets the financed amount by subtracting the down payment amount from the initial price
		public Double calculateFinancedAmount() {
			return getPrice() - calculateDownPayment();
		}
		
		//uses an algorithm to discover monthly mortgage payments
		public Double calculateMonthlyPayment() {
			return (calculateFinancedAmount() * ((getRate()/100) / 12)) / 
					(1 - (Math.pow((1 + ((getRate()/100) / 12)), -(12 * getTerm()))));
		}
		
		/*
		 * gets amount paid by multiplying the monthly payments by the term of the loan times 12 
		 * and then adding the down payment 
		 */
		public Double calculateTotalAmountPaid() {
			return (12 * getTerm()) * calculateMonthlyPayment() + calculateDownPayment();
		}
		
		//gets total interest by subtracting the initial price from the total amount paid
		public Double calculateTotalInterestPaid() {
			return calculateTotalAmountPaid() - getPrice();
		}

		/*
		 * overridden toString method to print out monthly payments, total amount paid, and total interest paid
		 * each value is formatted to have only 2 decimal places
		 */
		@Override
		public String toString() {
			DecimalFormat df = new DecimalFormat("#.##");
			String string = "\nThe monthly mortgage payment is: $" + df.format(calculateMonthlyPayment()) +
							"\nThe total amount paid is: $" + df.format(calculateTotalAmountPaid()) + 
							"\nThe total interest paid is: $" + df.format(calculateTotalInterestPaid());
			return string;
		}

		//returns a new Mortgage object with the same parameters as the old one
		@Override
		protected Object clone() throws CloneNotSupportedException {
			return new Mortgage(this.getPrice(), this.getDownPayment(), this.getRate(), this.getTerm());
		}

		//compares other Mortgage objects by comparing the monthly mortgage payment
		@Override
		public int compareTo(Object arg0) {
			if(arg0.getClass().getName() == this.getClass().getName()){
				Mortgage m = (Mortgage) arg0;
				if(this.calculateMonthlyPayment() > m.calculateMonthlyPayment()) return 1;
				else if(this.calculateMonthlyPayment() < m.calculateMonthlyPayment()) return -1;
				else return 0;
			}
			else {
			System.out.println("You shouldn't compare a Mortgage object to another type of object.");
			return -9999;
			}
		}
		
		/**************************************************************/
		
	}//end Mortgage class

}//end Driver class
